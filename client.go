package main

import (
	"io"
	"log"
	"net"
	"time"
        "math/rand"
        "fmt"
)

func reader(r io.Reader) {
	buf := make([]byte, 1024)
	for {
		n, err := r.Read(buf[:])
		if err != nil {
			return
		}
		println("Client got:", string(buf[0:n]))
	}
}

func main() {
	c, err := net.Dial("unix", "/tmp/go.sock")
	if err != nil {
		log.Fatal("Dial error", err)
	}
	defer c.Close()

        seed := rand.NewSource(time.Now().UnixNano())
        r := rand.New(seed)
        id := r.Intn(100)

	go reader(c)
	for {
                msg := fmt.Sprintf("Client %d generated: %d", id, r.Intn(100))
		_, err := c.Write([]byte(msg))
		if err != nil {
			log.Fatal("Write error:", err)
			break
		}
		println("Client sent:", msg)
		time.Sleep(1e9)
	}
}
