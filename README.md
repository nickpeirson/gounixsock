# Proof of concept for Unix Domain Sockets in Go

Expected output:

## Terminal 1
```bash
$ go run server.go
2018/11/27 15:37:06 Starting echo server
Server got: Client 20 generated: 85
Server got: Client 56 generated: 41
Server got: Client 20 generated: 50
Server got: Client 56 generated: 5
Server got: Client 20 generated: 81
Server got: Client 56 generated: 6
Server got: Client 20 generated: 83
Server got: Client 56 generated: 87
```

## Terminal 2
```bash
$ go run client.go
Client sent: Client 20 generated: 85
Client got: Client 20 generated: 85
Client sent: Client 20 generated: 50
Client got: Client 20 generated: 50
Client sent: Client 20 generated: 81
Client got: Client 20 generated: 81
Client sent: Client 20 generated: 83
Client got: Client 20 generated: 83
```

## Terminal 3
```bash
$ go run client.go
Client sent: Client 56 generated: 41
Client got: Client 56 generated: 41
Client sent: Client 56 generated: 5
Client got: Client 56 generated: 5
Client sent: Client 56 generated: 6
Client got: Client 56 generated: 6
Client sent: Client 56 generated: 87
Client got: Client 56 generated: 87
```